import java.io.*;
import java.util.Scanner;

public class scriptAnalyzer {

	//count force 
	public static void countForce(int countF,String fileName) //scan file for word "force"
	{
		try
		{
			Scanner fileScanner = new Scanner(new File(fileName));
			while (fileScanner.hasNextLine())
			{
				String nextLine = fileScanner.next();//each line taken in  
				nextLine=nextLine.toLowerCase(); //converted to lower case to ignore case sensitivity 
				if (nextLine.contains("force"))
				{
					countF++; //add count for each instance of the word 
				}
			}
			fileScanner.close(); //close to prevent leak 
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage()); //if file name inputed wrong 
		}
		System.out.println("Number of instance of the word Force is "+countF); //print count to console 
	}
	
	//count wookie
	public static void countWookie(int countW,String fileName)  
	{
		try
		{
			Scanner fileScanner = new Scanner(new File(fileName));
			while (fileScanner.hasNextLine())
			{
				String nextLine = fileScanner.next(); 
				nextLine=nextLine.toLowerCase(); //convert to lower case and reassign to nextLine
				if (nextLine.contains("wookie"))
				{
					countW++;
				}
			}
			fileScanner.close(); //close to prevent leak 
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		System.out.println("Number of instance of the word Wookie is "+countW);
	}
	//count jabba
	public static void countJabba(int countJ,String fileName) 
	{
		try
		{
			Scanner fileScanner = new Scanner(new File(fileName));
			while (fileScanner.hasNext()) //as long as file has a new line it will continue to loop and look for next value
			{
				String nextLine = fileScanner.next();
				nextLine=nextLine.toLowerCase();
				if (nextLine.contains("jabba"))
				{
					countJ++;
				}
			}
			fileScanner.close();
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		System.out.println("Number of instance of the word Jabba is "+countJ);
	}
	//count death star
	public static void countDeathS(int countDS,String fileName)  
	{
		try
		{
			Scanner fileScanner = new Scanner(new File(fileName));
			while (fileScanner.hasNext())
			{
				String nextLine = fileScanner.nextLine();
				nextLine=nextLine.toLowerCase();
				if (nextLine.contains("death star")) //.contains ignores punctuation asscoiated with the word when checking value   
				{
					countDS++;
				}
			}
			fileScanner.close();
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		System.out.println("Number of instance of the word Death Star is "+countDS);
	}
	
	//main method 
	public static void main(String[] args) 
	{		
		Scanner keyboard = new Scanner(System.in); 
		System.out.println("Enter name of document"); //take in file name and apply to each method above
		String fileName = keyboard.nextLine(); 
		System.out.println("Scanning "+fileName); //output that command is being executed
		//call each method and return its values 
		scriptAnalyzer.countForce(0, fileName);  
		scriptAnalyzer.countWookie(0, fileName);
		scriptAnalyzer.countJabba(0,fileName);
		scriptAnalyzer.countDeathS(0,fileName);
	}
}

