
public class MonthException extends Exception{
	
	public MonthException()
	{
		super("Incorrect Month"); 
		//if month not formatted correctly or out of bounds (1-12)
	}
	public MonthException(String aMsg)
	{
		super (aMsg);
	}

}
