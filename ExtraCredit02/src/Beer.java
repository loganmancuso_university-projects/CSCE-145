
public class Beer {

	//instance variables
	private String name;
	private double alcoholContent;
	
	//default constructor 
	public Beer()
	{
		beers = new Beer[BEER_SIZE];
		this.name = "Ne name Set";
		this.alcoholContent = 0.0;
	}
	//parameterized
	public Beer(String aName, double aContent)
	{
		this.setName(aName);
		this.setContent(aContent);
	}
	
	//accessors
	public String getName()
	{
		return this.name;
	}
	public double getContent()
	{
		return this.alcoholContent;
	}
	//mutator
	public void setName(String aName)
	{
		this.name= aName;
	}
	public void setContent(double aContent)
	{
		if (aContent<5.0 && aContent>0.0)//beer is between .05 and 5%
		{
			this.alcoholContent= aContent;
		}
		else if(aContent>5.0) 
		{
			System.out.println("That is too strong to be a beer!");
		}
		else if(aContent<0.0)
		{
			System.out.println("Thats not alcoholic!");
			this.alcoholContent = aContent;
		}
	}
	//other methods
	private Beer[] beers;
	public static final int BEER_SIZE = 2;
	
	public void addBeer(Beer aBeer)
	{
		for (int i=0; i<BEER_SIZE;i++)
		{
			if (beers[i]==null)
			{
				beers[i] = aBeer;
				break;
			}
		}
	}
	
	public void Intoxicated(double weight)
	{
		for (int i=0; i<2; i++)
		{
			double calculation = ((0.08+0.015)*weight)/(12*7.5*beers[i].getContent()); //calculation 
			int numbOfBeers = (int)calculation; //round to nerest whole "beer"
			if (numbOfBeers ==0) //if the number is less than one and rounds to zero it will be close to one beer to be intoxicated
			{
				numbOfBeers = 1;
				System.out.println("It would take less than "+ numbOfBeers +" full beer(s) of "+ beers[i].getName().trim() + "'s to be intoxicated" );
			}
			else 
			{
				System.out.println("It would take approximatly "+ numbOfBeers +" full beer(s) of "+ beers[i].getName().trim() + "'s to be intoxicated" );
			}
		}
		System.out.println("\n");
	}
}
