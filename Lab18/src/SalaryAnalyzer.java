import java.io.*;
import java.util.*;
public class SalaryAnalyzer {
	static final String IN_FILE_NAME = "StateOfSC-Salary-List-04012015.txt";
	static final String OUT_FILE_NAME = "OVER250000.txt";
	static final String DELIM = "\t";
	public static void main(String[] args)
	{
		System.out.println("Let's see how many state employees make over $250,000 and work at USC.");
		analyzeEmployeeFile(IN_FILE_NAME);
		System.out.println("Results have been printed to "+OUT_FILE_NAME);
	}
	public static void analyzeEmployeeFile(String fileName)
	{
		//TODO: Fill this in
		try
		{
			Scanner fileScanner = new Scanner(new File(fileName));
			fileScanner.nextLine();
			String result = "";
			while (fileScanner.hasNextLine())
			{
				String fileLine = fileScanner.nextLine();
				String[] splitLines = fileLine.split(DELIM); //split line based on DELIM = \t
				double totalIncome = Double.parseDouble(splitLines[splitLines.length-1]);

				if (totalIncome>=250000) //find employees with Total Income >= 250,00
				{
					System.out.println(fileLine);
					result = result.concat("\n"+ fileLine);
				}
			}
			printToSalaryFile(OUT_FILE_NAME,result);
			fileScanner.close();
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage()); //if error occurs 
		}
	}
	public static void printToSalaryFile(String fileName, String text)
	{
		//TODO: Fill this in
		try
		{
			PrintWriter fileWriter = new PrintWriter(new FileOutputStream(fileName)); 
			fileWriter.println(text); //print to file
			fileWriter.close(); //close stream 
		}
		catch (Exception e) //if error
		{
			System.out.println(e.getMessage());
		}
	}
}
