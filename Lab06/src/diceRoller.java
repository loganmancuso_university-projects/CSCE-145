/* Mancuso, Logan 
* This program will simulate rolling a device a set number of times 
* as determined by the user and out put each roll and the tally of the 
* number of times each time a side was landed on 
*/
import java.util.Scanner;
import java.util.Random;

public class diceRoller {
	public static final int randInt = 7;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//setting the min and max range of random numbers
		int min = 1; 
		int max = 6;
		
		System.out.println("Enter the number of times a die should be rolled");
		Scanner keyboard = new Scanner(System.in);
		int numberOfRolls = keyboard.nextInt(); //storing user input rolls
		int startLoop = 1; //setting value of were the for loop will start
		//setting the value of each tally at zero
		int numberOfOne = 0;
		int numberOfTwo = 0;
		int numberOfThree = 0;
		int numberOfFour = 0;
		int numberOfFive = 0;
		int numberOfSix = 0;
		
		if (numberOfRolls <= 0) //if user inputs a negative amount of rolls 
		{
			System.out.println("not a valid input");
			System.exit(0);
		} 
			for (startLoop = 1; startLoop <= numberOfRolls; startLoop++)// for loop with user inputed set number of loops
			{
				Random r = new Random();//choosing a random number rolled by die 
				int dieNumber = r.nextInt(max-min +1) + min;
				System.out.println(dieNumber + " was rolled");	
				/* adding up the values of each roll
				* if a roll is equal to one of the values below it 
				* adds one to the value of the tally associated with the 
				* number beneath it
				*/
				if (dieNumber == 1)
				{
					numberOfOne ++;
				}
				else if (dieNumber == 2)
				{
					numberOfTwo ++;
				}
				else if (dieNumber == 3)
				{
					numberOfThree ++;
				}
				else if (dieNumber == 4)
				{
					numberOfFour ++;
				}
				else if (dieNumber == 5)
				{
					numberOfFive ++;
				}
				else if (dieNumber == 6)
				{
					numberOfSix ++;
				}
			
		}
		//out put line with each number possible and how many were rolled of each
		System.out.println("_______ Tally _______");
			System.out.println(" 1 was rolled "+ numberOfOne + " time(s) \n"
					+" 2 was rolled "+ numberOfTwo + " time(s) \n"
					+" 3 was rolled "+ numberOfThree + " time(s) \n"
					+" 4 was rolled "+ numberOfFour + " time(s) \n"
					+" 5 was rolled "+ numberOfFive + " time(s) \n"
					+" 6 was rolled "+ numberOfSix + " time(s)");			
	}

}
