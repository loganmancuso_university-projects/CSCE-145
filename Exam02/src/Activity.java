
public class Activity {

	//instance variables 
	private String name;
	private int startHour;
	private int endHour;
	
	//default 
	public Activity()
	{
		this.name="no name set";
		this.startHour = 0;
		this.endHour = 23;
	}
	
	//parameterized 
	public Activity(String aName, int aStartHour, int anEndHour)
	{
		//use mutator
		this.setName(aName);
		this.setStartHour(aStartHour);
		this.setEndHour(anEndHour);
	}
	
	//accessor 
	public String getName()
	{
		return this.name;
	}
	public int getStartHour()
	{
		return this.startHour;
	}
	public int getEndHour()
	{
		return this.endHour;
	}
	
	//mutator
	public void setName(String aName)
	{
		this.name = aName;
	}
	public void setStartHour(int aStartHour)
	{
		if (aStartHour >= 0 && aStartHour <=23)
		{
			this.startHour = aStartHour;
		}
		else 
		{
			System.out.println("Invalid start hour");
		}
	}
	public void setEndHour(int anEndHour)
	{
		if (anEndHour >= 0 && anEndHour <=23)
		{
			this.endHour = anEndHour;
		}
		else 
		{
			System.out.println("Invalid end hour");
		}
	}
	
	//other methods
	public void toAString(String activity)
	{
		activity = ("Activity: "+ getName()+ " Start Time: "+ getStartHour()+ " End Time: "+ getEndHour());
		return;
	}
}
