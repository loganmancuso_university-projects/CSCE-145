/*Mancuso, Logan
 * this program will take user inputed names and sort them based on first letter of the last name
 */
import java.util.Scanner;
public class alphaSorting {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int i;
		String temp;
		
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Name sorting program");
		String [] arrayName = new String [10];
		//begin populating array
		for (i=0; i<10; i++)
		{
			System.out.println("Enter a Name: first and last with a space");
			String Name = keyboard.nextLine();
			arrayName[i] = Name; //array of the names	
		}
		//Sort the array  
		boolean swapped = true;
		while (swapped)
		{
			swapped = false;//none have been swapped list is in order
			for (i=0; i<10-1; i++)
			{
				int spaceLastNameOne = arrayName[i].indexOf(" ");//find the space separating first and last name 
				int spaceLastNameTwo = arrayName[i+1].indexOf(" ");
				char letterAfterSpaceOne = arrayName[i].charAt(spaceLastNameOne+1);//assign the character after that space 
				char letterAfterSpaceTwo = arrayName[i+1].charAt(spaceLastNameTwo+1);
				if (letterAfterSpaceOne>letterAfterSpaceTwo)//if the character is greater than the second index last name perform then swap 
				{
					temp = arrayName[i]; //swap using temp value 
					arrayName[i] = arrayName[i+1];
					arrayName[i+1] = temp;
					swapped = true;
				}
			}
		}
		//display sorted list
		System.out.println("The sorted list is: \n");
		for (i=0; i<10; i++)
		{
			System.out.print(arrayName[i]+", ");
		}
	}

}
