/*Mancuso, Logan
 * this program will take an input from the user to determine the dimensions of 
 * a matrix then populate and perform a function between the two
 * it will also check for compatibility between the sizes of the matrix 
 */
import java.util.Scanner;

public class Matrix {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		System.out.println("Enter the number of rows and columns in matrix one (seperated by a space)");
		Scanner keyboard = new Scanner(System.in);
		int rowOne = keyboard.nextInt(); //height for matrix one
		int colOne = keyboard.nextInt(); //width for matrix one
		System.out.println("Enter the number of rows and columns in matrix two (seperated by a space) ");
		int rowTwo = keyboard.nextInt(); //height for matrix two
		int colTwo = keyboard.nextInt(); //width for matrix two
		//check dimensions of array
		if ((rowOne != rowTwo) || (colOne != colTwo))//if either rows or column do not match program terminates 
		{
			System.out.print("Matrix cannot be combined dimesion mismatch \n"
					+ "system will exit");
			System.exit(0); 
		}
		//create multidimensional array from user input dimensions 
		double [][] matrixOne = new double[rowOne][colOne];
		double [][] matrixTwo = new double[rowTwo][colTwo];
		double [][] matrixCombined = new double [rowOne][colTwo];
		//for loop to populate array number one
		for (int i=0; i<rowOne; i++) //row
		{
			for (int j=0; j<colOne; j++) //column
			{
				System.out.println("Enter the data for the first matrix at ("+ (i+1) + "," + (j+1)+")");
				double data = keyboard.nextDouble();// input number
				matrixOne[i][j] = data;
			}
		} 
		
		//for loop to populate array number two
		for (int i=0; i<rowTwo; i++) //row
		{
			for (int j=0; j<colTwo; j++) //column
			{
				System.out.println("Enter the data for the second matrix at ("+ (i+1) + "," + (j+1)+")");
				double data = keyboard.nextDouble();// input number
				matrixTwo[i][j] = data;
			}
		}
		//promp user for type of operation
		System.out.println("what operation do you want to preform? \n "
							+ "add, mult, div, sub");
		Scanner userOpperation = new Scanner(System.in);
		String operation = userOpperation.next();
		//print first matrix
		System.out.println("Matrix One is:");
		for (int i=0; i<rowOne; i++)
		{
			for (int j=0; j<colOne; j++)
			{
				System.out.print(matrixOne [i][j]+"\t");
			}
			System.out.println("\n");
		}
		//print second matrix
		System.out.println("Matrix Two is:");
		for (int i=0; i<rowOne; i++)
		{
			for (int j=0; j<colOne; j++)
			{
				System.out.print(matrixTwo [i][j]+"\t");
			}
			System.out.println("\n");
		}
		//print operation performed between matrix 
		System.out.print("The " + operation + " Matrix is: \n");
		//create switch case based on chosen operation
		switch (operation)
		{
			case "add": //addition 
			{
				for (int i=0; i<rowOne; i++)
				{
					for (int j=0; j<colOne; j++)
					{
						matrixCombined [i][j] = matrixOne[i][j] + matrixTwo[i][j];
						System.out.print(matrixCombined [i][j]+"\t");
					}
					System.out.println("\n");
				}
			break;
			}
			case "mult": //multiplication
			{
				for (int i=0; i<rowOne; i++)
				{
					for (int j=0; j<colOne; j++)
					{
						matrixCombined [i][j] = matrixOne[i][j] * matrixTwo[i][j];
						System.out.print(matrixCombined [i][j]+"\t");
					}
					System.out.println("\n");
				}
				break;
			}// wont work for all cases dimensions can not match but still be multiplied
			case "div": //division
			{
				for (int i=0; i<rowOne; i++)
				{
					for (int j=0; j<colOne; j++)
					{
						matrixCombined [i][j] = matrixOne[i][j] / matrixTwo[i][j];
						System.out.print(matrixCombined [i][j]+"\t");
					}
					System.out.println("\n");
				}
				break;
			}// wont work for all cases dimensions can not match but still be divided
			case "sub": //subtraction
			{
				for (int i=0; i<rowOne; i++)
				{
					for (int j=0; j<colOne; j++)
					{
						matrixCombined [i][j] = matrixOne[i][j] - matrixTwo[i][j];
						System.out.print(matrixCombined [i][j]+"\t");
					}
					System.out.println("\n");
				}
				break;
			}	
		}// end of switch statement 
	}
}
