/*
 *Mancuso, Logan 
 *create a story with 3 different endings 3 different decisions and at least one comparison of numbers and strings
 *this story has one beginning and four endings, the two sets of endings are the young boy will get to the wise professor 
 *he will be sucked into the matrix or he will get hit by the train or his notebook will be destroyed and he has to return home  
  */
import java.util.Scanner;
public class makeYourOwnStory {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard =new Scanner(System.in);
		//Beginning
		System.out.println("there was once a young boy learning to become a programmer, his skills were mediocre and he still had \n"  
				+ "much to learn. So he ventured upon a quest to create the most incredible program story.\n"
				+ "He had heard of a wise man who lived deep within Mt Coker. This man was willing to teach all that he knew to \n"
				+ "the few brave souls who dared to venture to the mountain. So the young programmer set forth \n"
				+ "upon his journey to Mt Coker to meet the wise professor. He knew the journey would be difficult \n"
				+ "so he braced himself for all that he might encounter and sure enough as \n"
				+ "he was travelling along he came to an intersection. A troll suddenly apeared, 'young boy' he screached 'why do you come this way'\n "
				+ "'I seek the guidance of the great professor JJ' the boy stammered 'well this way is the quickest if you can answer me this'\n"
				+ "'what is your question troll' said the boy. 'I am thinking of a number between 1 and 10, choose wisely and be quick'");
		//Story section one
		System.out.println("choose a number between 1 and 10"); //decision one two outcomes r1 or r2
			int responseOne = keyboard.nextInt();
			{	
			if ((responseOne > 10) || (responseOne < 1)){
					System.out.println("that is not a valid number");
				}
			if ((responseOne >=1) && (responseOne <=10) && (responseOne !=4)){ //r1
					System.out.println("you are wrong find another way, the young programmer was sad that he could \n not guess the right answer so he decided to hide and wait "
							+ "for the troll to move and he snuck past him"); //more story here
					System.out.println("The programmer was worried as night began to fall but he soon came to a \n set of train tracks near the edge of the Swearingen forest"
							+ "as the sound of a passing train faded away he had an idea, approaching a hooded figure acroos the street he asked 'do you know a faster rute to"
							+ "Mt Coker?' why yes I do and there are two ways you could go, follow this path \n forward and possibly face another obsticle or go under and around the mountain");
						System.out.println("will you continue on your path or go around?, please state forward or around"); //more story line 
							Scanner keyboardFive = new Scanner(System.in); //next input
							String responseFive = keyboardFive.nextLine(); //two answers r1.1 and r1.2
								String forward = "forward"; //r1.1
								String around = "around"; //r1.2
							if (responseFive.equalsIgnoreCase(forward)){ //r1.1
								System.out.println("with a new found purpose the young programmer continued on his quest and after a while he made it to Mt Coker where he found two identical"
										+ "doors, he knew that one would take him to where he desired the other to an imminent ending");
								System.out.println("Do you choose door 1 or 2");
								Scanner keyboardSeven = new Scanner(System.in); //next input
									int responseSeven = keyboardSeven.nextInt(); //two answers r1.2.1 and r1.2.2
										if (responseSeven == 1){ //r1.2.1 
											System.out.println("congratulations you made it to the class now bask in the knowledge of the nerds. The End"); //ending
										}
										if (responseSeven == 2){//r1.2.2
											System.out.println("you have been sucked into a black hole and transported to the Matrix The End"); //ending
										}
							}
							if (responseFive.equalsIgnoreCase(around)) //r1.2
								System.out.println("the young programmer knew that the obsitcles ahead could be \n difficult and being wary of the path he decided to venture around "
									+ "where he was able to make it to Mt Coker safetly and as he turned the corner their were two identical doors "
									+ "one would lead to the wise professor and the other a vast unknown world"
									+ "do you choose door 1 or 2"); //more story line
								Scanner keyboardSeven = new Scanner(System.in); //next input
									int responseSeven = keyboardSeven.nextInt(); //two answers r1.2.1 and r1.2.2
										if (responseSeven == 1){ //r1.2.1 
											System.out.println("congratulations you made it to the class now bask in the knowledge of the nerds. The End"); //ending
										}
										if (responseSeven == 2){//r1.2.2
											System.out.println("you have been sucked into a black hole and transported to the Matrix The End"); //ending
										}
				}
			if ((responseOne == 4)){ //r2
						System.out.println("good you may pass");
						System.out.println("So the young programmer continued on his way proud of his accomplishment, little did he know that it would become much harder\n"
								+ "from here on out. As he ventured further toward the mountain the sun was begining to set and he came to a small clearing near the Swearingen forset. \n"
								+ "when all of the sudden another figure aproached him in a hurried voice he told the boy ' a train is comming if you are quick you can cross the train \n"
								+ "or you can venture slightly off course and go underneath, this will take time and you may not reach the mountain before night fall.");
							System.out.println("will you try to cross or go under?, please state cross or under"); //more story line 
								Scanner keyboardTwo = new Scanner(System.in); //next input
								String responseTwo = keyboardTwo.nextLine(); //two answers r2.1 and r2.2
									String cross = "cross"; //r2.1
									String under = "under"; //r2.2
								if (responseTwo.equalsIgnoreCase(cross)){ //r2.1
									System.out.println("with a burst of (non existant)speed the young (non athletic major nerd attempted) to run across the tracks\n"
										+ "but he tripped and fell across the tracks leaving his notebook on the otherside, with the train whistling is his ears\n "
										+ "he had to make a quick decision does he try to get up and go back or cross the tracks and leave the notebook");
									System.out.println("please state, forward or back"); //more story
									Scanner keyboardFour = new Scanner(System.in);	
										String responseThree = keyboardFour.nextLine(); //two answers r2.1.1 and r2.1.2
											String forwardTwo = "forward"; //r2.1.1
											String back = "back";//r2.1.2
										if (responseThree.equalsIgnoreCase(forwardTwo)){ //r2.1.1
											System.out.println("as the train rushed by it destroyed the note book and "
													+ "without his notebook the young programmer could not take note of all that he would learn from "
													+ "the wise professor, so desheviled and beaten the boy returned home"); //ending
										}
										if (responseThree.equalsIgnoreCase(back)){ //r2.1.2
											System.out.println("the young programmer tragically go hit by the train. The End"); //ending
										}
								}
								if (responseTwo.equalsIgnoreCase(under)) //r2.2
									System.out.println("the young programmer knew that the train would probably win if he tried to cross he decided to go under\n "
										+ "where he was able to make it to Mt Coker safetly and as he turned the corner their were two identical doors\n "
										+ "one would lead to the wise professor and the other a vast unknown world\n"
										+ "do you choose door 1 or 2"); //more story line
									Scanner keyboardThree = new Scanner(System.in); //next input
										int responseFour = keyboardThree.nextInt(); //two answers r2.2.1 and r2.2.2
											if (responseFour == 1){ //r2.2.1 
												System.out.println("congratulations you made it to the class now bask in the knowledge of the nerds. The End"); //ending
											}
											if (responseFour == 2){//r2.2.2
												System.out.println("you have been sucked into a black hole and transported to the Matrix The End"); //ending
											}
					}
					
			}
			
	}
}


