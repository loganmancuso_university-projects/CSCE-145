/*
 * Mancuso, Logan 
 * This program will evaluate BMR (Basal Metabolic Rate) 
 * using nested if else statements i will be able to evaluate this for males and females 
 *  
 */
import java.util.Scanner;
public class BMRCalculator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//gender
		System.out.println("This program will calculate BMR \n"
				+ "please input your gender 1 for male 2 for female");
		Scanner genderInput =new Scanner(System.in);
		int gender= genderInput.nextInt();
		//weight
		System.out.println("Now input you weight in pounds");
		Scanner weightInput =new Scanner(System.in);
		double weight = weightInput.nextDouble();
		//height
		System.out.println("Now input your height in inches");
		Scanner heightInput =new Scanner(System.in);
		double height = heightInput.nextDouble();
		//age
		System.out.println("Now input your age in years");
		Scanner ageInput =new Scanner(System.in);
		double age = ageInput.nextDouble();
		//lifestyle
		System.out.println("How would you describe your lifestyle \n"
				+ "Sedentary, Somewhat_Active, Active, Highly_Active");
		Scanner lifestyleInput =new Scanner(System.in);
		String lifestyle = lifestyleInput.nextLine();
		final int userInputMale = 1;
		final int userInputFemale = 2;
		
		//all data has been inputed and stored, now to calculate
		//create switch case 
		if (gender >=1 && gender <=2);
		{
			switch (gender)	
			{
				case userInputMale: //if user inputs male
				{
					double maleBMR = (66+(6.3*weight))+(12.9*height)-(6.8*age); //calculation for BMR as male
						if (lifestyle .equalsIgnoreCase("Sedentary"))//lifestyles are as follows
						{
							double totalBmr = Math.round(maleBMR*1.20);
							System.out.println("Your BMR is " + totalBmr +"Calories");
						}
						else if (lifestyle .equalsIgnoreCase("Somewhat_Active"))
						{
							double totalBmr = Math.round(maleBMR*1.30);
							System.out.println("Your BMR is " + totalBmr +"Calories");
						}
						else if (lifestyle .equalsIgnoreCase("Active"))
						{
							double totalBmr = Math.round(maleBMR*1.40);
							System.out.println("Your BMR is " + totalBmr +"Calories");
						}
						else if (lifestyle .equalsIgnoreCase("Highly_Active"))
						{
							double totalBmr = Math.round(maleBMR*1.50);
							System.out.println("Your BMR is " + totalBmr +"Calories");
						}
					break;
				}
				case userInputFemale: //if user inputs female
				{
					double femaleBMR = (665+(4.3*weight))+(4.7*height)-(4.7*age); //calculation for BMR as female
						if (lifestyle .equalsIgnoreCase("Sedentary"))//lifestyles are as follows
						{
							double totalBmr = Math.round(femaleBMR*1.20);
							System.out.println("Your BMR is " + totalBmr +"Calories");
						}
						else if (lifestyle .equalsIgnoreCase("Somewhat_Active"))
						{
							double totalBmr = Math.round(femaleBMR*1.30);
							System.out.println("Your BMR is " + totalBmr +"Calories");
						}
						else if (lifestyle .equalsIgnoreCase("Active"))
						{
							double totalBmr = Math.round(femaleBMR*1.40);
							System.out.println("Your BMR is " + totalBmr +"Calories");
						}
						else if (lifestyle .equalsIgnoreCase("Highly_Active"))
						{
							double totalBmr = Math.round(femaleBMR*1.50);
							System.out.println("Your BMR is " + totalBmr +"Calories");
						}
					break;
				}
			}//end switch
		}
	}
		

}
