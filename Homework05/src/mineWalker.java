/*Mancuso, Logan
 * this program will place a user on a board at location (0,0) 
 * and set a random point as the goal and other random points as 
 * mines on the game board "field" the user then inputs coordinates 
 * for each move and will try to reach the goal without landing on a mine
 * 
 *  (Easy is the requirements of the assignment, board of 10x10 and 
 *  mines that cover 10% of board)
 *  
 *  easy -> board of 10x10 10% mines
 *  medium -> board of 20x20 25% mines
 *  hard -> board of 30x30 35% mines
 */
import java.util.Scanner;
import java.util.Random;

public class mineWalker {

enum Spaces {Empty,Player,Mine,Goal,Path};
public static final int BOARD_SIZE_EASY = 10;
public static final int BOARD_SIZE_MEDIUM = 20;
public static final int BOARD_SIZE_HARD = 30;

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int y;
		int x;
		System.out.println("Welcome to Mine Walker choose your leve of dificulty\n"
				+ "easy (e), medium (m), or hard (h)");
		Scanner keyboard = new Scanner(System.in);
		String difficulty = keyboard.nextLine();
		switch (difficulty)
		{
			case "e":
			{
				//score ie number of moves, less moves better score
				int numbMoves = 0;
				//goal
				Random r = new Random();
				int gX = r.nextInt(BOARD_SIZE_EASY);
				int gY = r.nextInt(BOARD_SIZE_EASY);
				//player
				int pX = 0;
				int pY = 0;
				//mines
				int mX = r.nextInt(BOARD_SIZE_EASY);
				int mY = r.nextInt(BOARD_SIZE_EASY);
				//create board
				Spaces[][] board = new Spaces[BOARD_SIZE_EASY][BOARD_SIZE_EASY];
				//loop to initialize board 
				for (y=0; y<board.length; y++)
				{
					for (x=0; x<board[y].length; x++)
					{
						board[x][y] = Spaces.Empty;
					}
				}
				//loop to place mines 
				for (y=0; y<(board.length/10); y++)
				{
					for (x=0; x<(board[y].length/10); x++)
					{
						board[x][y] = Spaces.Mine;
					}
				}
				//place player
				board[pX][pY] = Spaces.Player;
				//place goal
				board[gX][gY] = Spaces.Goal;
				//game
				boolean gameOver = false;
				
				while (gameOver == false)
				{
					//print board to screen
					for (y=0; y<board.length; y++)
					{
						for (x=0; x<board[y].length; x++)
						{
							switch(board[x][y])
							{
								case Empty:
								{
									System.out.print("_");
									break;
								}//end of case empty space
								case Goal:
								{
									System.out.print("*");
									break;
								}//end of case goal space
								case Player:
								{
									System.out.print("x");
									break;
								}//end of case player space
								case Path:
								{
									System.out.print("x");
									break;
								}
								case Mine:
								{
									System.out.print("M");
									break;//change once error checking is done
								}
								default:
								{
									System.out.print("error");
									break;
								}
									
							}
							System.out.print("");
						}
					}
				}//end of while loop
				
				
				break;
			}//end of easy 
			case "m":
			{
				
				break;
			}//end of medium
			case "h":
			{
				
				break;
			}//end of hard
			default:
			{
				System.out.println("Invalid difficulty system exit");
				System.exit(0);
			}
		}
		
	}

}
