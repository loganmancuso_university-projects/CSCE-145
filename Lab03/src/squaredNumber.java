/*
 * Mancuso, Logan Program that will make a box of a 4 digit number.
 * First take the input and copy the input
 * Top line done now to go down the line and separate the characters, 
 * taking the first in line and the second in line out and store as separate variables.  
 * Now to reverse the original input 
 * take the original, make a copy and then using the 'reverse' function 
 * Rearrange the characters in reverse order and store again as variable 
 * fix the System.out.println function to directly display the numbers in a box using spaces and "" marks to add spaces.
 */
import java.util.Scanner;
public class squaredNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter a 4 digit number");
		Scanner keyboard = new Scanner(System.in);
		//Convert first line to a string after input from long
			long workingInput = keyboard.nextLong();
			long topLine = workingInput;
		/*
		 * Top line done now to go down the line and separate the characters, 
		 * taking the first in line and the second in line out and store as separate variables.  
		 */
			String Input = Long.toString(workingInput);
			char secondChar = Input.charAt(1);
			char thirdChar = Input.charAt(2);
		/*
		 * Now to reverse the original input 
		 * take the original, make a copy and then using the 'reverse' function 
		 * Rearrange the characters in reverse order and store again as variable 
		 */
			String copyInput = Input;
			String reverse = "";
			int length = copyInput.length();
			for (int i = length - 1 ; i >= 0 ; i--)
				reverse = reverse + copyInput.charAt(i);
		//Here is the final output, label that this is an output then print out the lines	
			System.out.println("Here is your 'squared' number");
			System.out.println(topLine);
			System.out.println(secondChar +"  " + thirdChar );
			System.out.println(thirdChar  + "  " + secondChar);
			System.out.println(reverse);
				
	}

}
