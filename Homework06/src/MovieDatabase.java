import java.util.Scanner;
import java.io.*;
public class MovieDatabase {
	
	private Movie[] movies;
	public static final int MOVIEDB_SIZE = 30;
	public static final String delim = "\t";
	public static final int FIELD_AMT = 5;
	
	public MovieDatabase()
	{
		movies = new Movie[MOVIEDB_SIZE];
	}
	public Movie[] getMovie()
	{
		return this.movies;
	}
	
	/*
	 * adding and removing 
	 */
	
	//add a movie 
	public void addMovie(Movie aMovie)
	{
		for (int i=0; i<MOVIEDB_SIZE;i++)
		{
			if (movies[i]==null)
			{
				movies[i] = aMovie;
				break;
			}
		}
	}
	
	//remove movie by title
	public void removeMovie(String aName) throws IndexException
	{
		try
		{
			int removeIndex = -1; //invalid index use for when movie not found
			for (int i=0; i<movies.length;i++)
			{
				if (movies[i].getName().equals(aName))//if the name matches the one inputed 
				{
					removeIndex = i; //set index to one that will be removed 
					break;
				}
			else if(movies[i]==null)
				{
					System.out.println("Movie was not found");
					break;
				}
			}
			if (removeIndex == -1) 
			{
				//throw name not found at index i exception 
				throw new IndexException();
			}
			else 
			{
				//push all other values up one value to overwrite
				for (int i=removeIndex;i<movies.length-1;i++)
				{
					movies[i] = movies[i+1];
				}
				movies[movies.length-1] = null; //last values set to null when removed and shifted or you will have a duplicate of last value 
				System.out.println("Movie was removed");	
			}
			
		}
		catch (IndexException e)
		{
			System.out.println(e.getLocalizedMessage());
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	
	/*
	 * Sorting (title, rating, box office gross, & director by Last Name)
	 */
	
	//sort movie by title
	public void sortByTitle() //bubble sort by title
	{
		boolean swapped = true;
		while(swapped == true)
		{
			swapped = false;
			for(int i=0;i<MOVIEDB_SIZE-1;i++)
			{
				if(movies[i+1]==null)
				{
					break;
				}
				if(movies[i].getName().toUpperCase().trim().compareTo(movies[i+1].getName().toUpperCase().trim())>0)//Out of order swap add .trim() to remove white spaces (exra caution)
				{
					Movie temp = movies[i];
					movies[i] = movies[i+1];
					movies[i+1] = temp;
					swapped = true;
				}
			}
		}
		
	}
	
	//sort movie by rating 
	public void sortByRating() //bubble sort by rating
	{
		boolean swapped = true;
		while(swapped == true)
		{
			swapped = false;
			for(int i=0;i<MOVIEDB_SIZE-1;i++)
			{
				if(movies[i+1]==null)
				{
					break;
				}
				if(movies[i].getRating() >  movies[i+1].getRating())//Out of order
				{
					Movie temp = movies[i];
					movies[i] = movies[i+1];
					movies[i+1] = temp;
					swapped = true;
				}
			}
		}
	}
	
	//sort movie by BOG
	public void sortByBOG() //bubble sort by box office gross
	{
		boolean swapped = true;
		while(swapped == true)
		{
			swapped = false;
			for(int i=0;i<MOVIEDB_SIZE-1;i++)
			{
				if(movies[i+1]==null)
				{
					break;
				}
				if(movies[i].getBoxOffGross()>movies[i+1].getBoxOffGross())//Out of order swap
				{
					Movie temp = movies[i];
					movies[i] = movies[i+1];
					movies[i+1] = temp;
					swapped = true;
				}
			}
		}
	}
	
	//sort by Director
	public void sortByDirector() //bubble sort by director
	{
		boolean swapped = true;
		while(swapped == true)
		{
			swapped = false;
			for(int i=0;i<movies.length-1;i++)
			{
				if(movies[i+1]==null)
				{
					break;
				}
				if(movies[i].getDirector().toUpperCase().trim().compareTo(movies[i+1].getDirector().toUpperCase().trim())>0)//Out of order use .trim() to remove white space if last name same work on first name without space
				{
					Movie temp = movies[i];
					movies[i] = movies[i+1];
					movies[i+1] = temp;
				
					swapped = true;
				
				}
			}
		}
		
	}
	
	/*
	 * show movies by Director (screwed up and read directions as showing movies by a director 
	 * not to sort by director so i left this other method in because i worked hard on this code 
	 * probably should have read directions better) lines 190 to 222
	 */
	
	//have system scan the array to compare the user input <<"director name">> to the movie.getDirector value 
	public void showMoviesByDirector(String theDirector) throws DirectorNotFoundException
	{
		try
		{
			
			for (int i=0; i<movies.length;i++)
			{
				if(movies[i]==null)
				{
					break;
				}
				else if (movies[i].getDirector().equalsIgnoreCase(theDirector)) //find movies with director equal to user input director name
				{
					System.out.println(movies[i].getName()+delim+movies[i].getYear()
									+delim+movies[i].getRating()+delim+movies[i].getDirector()
									+delim+movies[i].getBoxOffGross());
				}
				else 
				{
					throw new DirectorNotFoundException();
				}
			}
		}
		catch (DirectorNotFoundException e)
		{
			System.out.println(e.getLocalizedMessage());
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage()); //if error occurs 
		}
	}
	
	/*
	 * File i/o (write to a file, & read from a file)
	 */
	
	//print DB to a file
	public void writeMovieDBFile(String aFileName)
	{
		try
		{
			PrintWriter fileWriter = new PrintWriter(new FileOutputStream(aFileName));
			for (int i=0;i<MOVIEDB_SIZE;i++)
			{
				if (movies[i] != null)
				{
					fileWriter.println(movies[i].getName()+delim+movies[i].getYear()+delim+
							movies[i].getRating()+delim+movies[i].getDirector()
							+delim+movies[i].getBoxOffGross());
				}
				else if(movies[i] == null)
				{
					//System.out.println("line empty no data"); error check remove after 
					break;
				}
				
			}
		fileWriter.close();
		}
		catch (Exception e) 
		{
			System.out.println(e.getMessage());
		}
	}
	
	//read from file
	public void readMovieDBFile(String fileName) throws FileFormatException
	{
		try
		{
			movies = new Movie[MOVIEDB_SIZE];
			Scanner fileScanner = new Scanner(new File(fileName));
			while (fileScanner.hasNextLine())
			{
				String fileLine = fileScanner.nextLine();
				String[] splitLines = fileLine.split(delim);
				if (splitLines.length == FIELD_AMT)
				{
					String name = splitLines[0];
					int year = Integer.parseInt(splitLines[1]);
					int rating = Integer.parseInt(splitLines[2]);
					String director = splitLines[3];
					double boxOffGross = Double.parseDouble(splitLines[4]);
					Movie aMovie = new Movie(name,year,rating,director,boxOffGross);
					
					this.addMovie(aMovie);
				}
				else 
				{
					throw new FileFormatException();
				}
			}
			fileScanner.close();
		}
		catch (FileFormatException e)
		{
			System.out.println(e.getLocalizedMessage());
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
//end
}
