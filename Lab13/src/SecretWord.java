//Mancuso, Logan 

public class SecretWord 
{
	//instance variables 
	private String secretWord;
	private String hintWord;
	private int numberOfTurns;
	
	//set default values for each variable 
	//constructors 
	public SecretWord() 
	{
	    this.secretWord = "fruit";
	    this.numberOfTurns = 0;
		this.hintWord = "*****"; //setup asterisk for each letter in secret word  
	}
	
	//accessors
	public String getSecretWord()
	{
	    return this.secretWord;
	}
	public String getHintWord()
	{
	    return this.hintWord;
	}
	public int getNumberOfTurns()
	{
	    return this.numberOfTurns;
	}
	
	//mutators
	public void setSecretWord(String aSecretWord) 
	{
		this.secretWord = aSecretWord;
	}
	public void setHintWord(String aHintWord)
	{
		this.hintWord = aHintWord;
	}
	public void setNumberOfTurns(int i)
	{
		if (i<0 || i>5)//for each turn count if it is not valid input 
		{
		    System.out.println("invalid");
		    return;
		}
		this.numberOfTurns = i;
	}
	public void guessLetter(char guess)
	{
		char[] tempHintWord = secretWord.toCharArray();//change hint word to a char array to compare value to secret word
		char[] hintWordArray = hintWord.toCharArray(); 
		for (int i = 0; i < tempHintWord.length; i++)
		{ 
			if (tempHintWord[i] == guess)
			{  
				hintWordArray[i]= guess;//if the guess is correct count the guess and set that letter in place of asterisk 
			}   
		}
		this.hintWord = String.valueOf(hintWordArray);
	}

}


